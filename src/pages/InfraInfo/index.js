import React from 'react'
import {
  Box,
  Text,
  // Link,
  Image,
  Button,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const Infra = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={5}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Informação de Infraestrutura
      </Text>

      <Box>
        <Container maxWidth='800px'>
          <Box flexDirection='column' justifyContent='center'>
            <Image
              src='https://miro.medium.com/max/940/1*uBFmEVpvj4fzF5HxLaAwMw.png'
              alt='Segun Adebayo'
            />
          </Box>
        </Container>
      </Box>

      <Box alignSelf='flex-end' mt={5} mr={5}>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download PDF
        </Button>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download Draw.io
        </Button>
      </Box>

      <Table my={100} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Componente
            </Th>
            <Th fontSize='medium' textColor='black'>
              Tipo
            </Th>
            <Th fontSize='medium' textColor='black'>
              Ambiente
            </Th>
            <Th fontSize='medium' textColor='black'>
              Host
            </Th>
            <Th fontSize='medium' textColor='black'>
              Detalhamento
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td fontWeight='bold'>MongoDB</Td>
            <Td>Banco de Dados</Td>
            <Td>Produção</Td>
            <Td>DIGPX12</Td>
            <Td>Detalhes adicionais sobre o componente</Td>
          </Tr>
        </Tbody>
      </Table>
    </Container>
  </div>
)

export default Infra
