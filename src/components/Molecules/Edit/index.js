import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
  pl: 'XSMALL',
  pr: 'XSMALL',
  pt: 'XSMALL',
  mt: 'XXSMALL',
};

export default ({ line, column, value, percent, border, onChange }) => {
  const [text, setText] = useState(value || '');

  const handleChange = useCallback(
    (e) => {
      onChange(line, column, e.target.value);
      setText(e.target.value);
    },
    [column, line, onChange]
  );

  return (
    <Atom.Input
      autofocus
      onChange={handleChange}
      value={text}
      textAlign="center"
      width={percent}
      height={32}
      border="1px solid"
      borderRight={border && '1px solid'}
      borderColor="white"
      borderRadius={5}
      bg="WHITE"
    />
  );
};
