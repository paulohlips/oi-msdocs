import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { color } from 'styled-system';
import styled from 'styled-components';
import { useAuth } from '../../../hooks';
// ui
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';
import ConfigOptions from '../ConfigOptions';
import { Imgs } from '../../../assets';
import MENU_DATA from './datamenu';

const ButtonRetract = styled.div`
  ${color}
  display: flex;
  align-items: center;
  justify-content: center;
  height: 35px;
  width: 35px;
  border-radius: 17.5px;
  position: absolute;
  left: ${({ retract }) => (retract ? 97 : 231)}px;
  bottom: 88%;
`;

export default () => {
  const { user } = useAuth();
  const { pathname } = useLocation();
  const [retract, setRetract] = useState(false);
  const rota = pathname.split('/');
  return (
    <Atom.Box p={retract && 0} variant="menu">
      {retract ? (
        <Atom.Avatar variant="retractedDrawer" src={Imgs.DEFAULT_PERFIL} />
      ) : (
        user && (
          <Atom.Flex variant="drawer">
            <Atom.Avatar
              variant="drawer"
              mb="XSMALL"
              src={Imgs.DEFAULT_PERFIL}
            />
            <Atom.Text variant="menu">{user.name}</Atom.Text>
            <Atom.Text variant="menu">{user.type}</Atom.Text>
          </Atom.Flex>
        )
      )}
      <Atom.Flex flexDirection="column" alignItems="flex-start">
        {MENU_DATA.map((item) => (
          <Link to={item.screen} style={{ textDecoration: 'none' }}>
            <Molecules.MenuItem
              retract={retract}
              text={item.label}
              icon={
                item.screen.split('/')[1] === rota[1]
                  ? item.iconSelected
                  : item.icon
              }
              selected={item.screen.split('/')[1] === rota[1] || null}
            />
          </Link>
        ))}
      </Atom.Flex>
      {retract ? <></> : <ConfigOptions />}
      {retract ? <Atom.BlackHole /> : <></>}

      <ButtonRetract
        bg="SECONDARY"
        retract={retract}
        onClick={() => setRetract(!retract)}
      >
        <Atom.Icon icon="ARROW_WHITE" invert={retract} />
      </ButtonRetract>
    </Atom.Box>
  );
};
