import React from 'react'
import {
  Box,
  Text,
  // Link,
  Image,
  Button,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const DataModel = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={5}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Modelo de Dados
      </Text>

      <Box>
        <Container maxWidth='800px'>
          <Box flexDirection='column' justifyContent='center'>
            <Image
              src='https://miro.medium.com/max/940/1*uBFmEVpvj4fzF5HxLaAwMw.png'
              alt='Segun Adebayo'
            />
          </Box>
        </Container>
      </Box>

      <Box alignSelf='flex-end' mt={5} mr={5}>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download PDF
        </Button>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download Draw.io
        </Button>
      </Box>

      <Table my={100} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Collection
            </Th>
            <Th fontSize='medium' textColor='black'>
              Informacoes sobre dados armazenados
            </Th>
            <Th fontSize='medium' textColor='black'>
              Detalhamento
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td fontWeight='bold'>WhatAppMessage</Td>
            <Td>
              industry. Lorem Ipsum has been the industrys standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing L
            </Td>
            <Td>
              <Tr>
                <Td fontWeight='black'>Ambiente</Td>
                <Td>PRODUCAO</Td>
              </Tr>
              <Tr>
                <Td fontWeight='black'>HOSTS</Td>
                <Td>DIGPX12, DIGPX13</Td>
              </Tr>
              <Tr>
                <Td fontWeight='black'>Database</Td>
                <Td>db_prd</Td>
              </Tr>
            </Td>
          </Tr>
        </Tbody>
      </Table>

      <Table mt={10} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Collection
            </Th>
            <Th fontSize='medium' textColor='black'>
              Informacoes sobre dados armazenados
            </Th>
            <Th fontSize='medium' textColor='black'>
              Detalhamento
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td fontWeight='bold'>WhatAppMessage</Td>
            <Td>
              industry. Lorem Ipsum has been the industrys standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing L
            </Td>
            <Td>
              <Tr>
                <Td fontWeight='black'>Ambiente</Td>
                <Td>PRODUCAO</Td>
              </Tr>
              <Tr>
                <Td fontWeight='black'>HOSTS</Td>
                <Td>DIGPX12, DIGPX13</Td>
              </Tr>
              <Tr>
                <Td fontWeight='black'>Database</Td>
                <Td>db_prd</Td>
              </Tr>
            </Td>
          </Tr>
        </Tbody>
      </Table>
    </Container>
  </div>
)

export default DataModel
