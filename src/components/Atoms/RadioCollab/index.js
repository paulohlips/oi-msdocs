import React from 'react';
import Text from '../Text';
import Container from '../Container';

export default ({ name, register, label, percent, onChange, ...props }) => {
  return (
    <Container flexDirection="column" width={`${percent}%`}>
      <Container mb={15} alignItems="flex-end">
        <Text ml={2}>{label}</Text>
      </Container>
      <div onChange={onChange}>
        <input type="radio" name="option" value="saude" /> Saúde
        <input
          type="radio"
          name="option"
          value="outros"
          style={{ marginLeft: '15px' }}
        />{' '}
        Outros
      </div>
    </Container>
  );
};
