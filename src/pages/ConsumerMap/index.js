import React from 'react'
import {
  Text,
  // Link,
  // Button,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const ConsumerMap = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container maxWidth='100%' borderRadius={4} mt={15}>
      <Text fontSize='large' fontWeight='extrabold'>
        GET
      </Text>
      <Text>/customers/cpf/products</Text>
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={2}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Mapeamento de Consumidores
      </Text>

      <Table my={10} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Canal
            </Th>
            <Th fontSize='medium' textColor='black'>
              Funcionalidade
            </Th>
            <Th fontSize='medium' textColor='black'>
              Descrição
            </Th>
            <Th fontSize='medium' textColor='black'>
              Consumo Médio Esperado
            </Th>
            <Th fontSize='medium' textColor='black'>
              Tipo de Acesso
            </Th>
            <Th fontSize='medium' textColor='black'>
              Usuário
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td>Minha Oi</Td>
            <Td>Login</Td>
            <Td>
              Utiliza as informações xpto no login para montar o dashboard do
              cliente com a visão de produtos
            </Td>
            <Td>Consumo médio esperado</Td>
            <Td>API Gateway Interno</Td>
            <Td>MOI</Td>
          </Tr>
          <Tr>
            <Td>Joice</Td>
            <Td>Login</Td>
            <Td>
              Utiliza as informações xpto no login para montar o dashboard do
              cliente com a visão de produtos
            </Td>
            <Td>Consumo médio esperado</Td>
            <Td>API Gateway Externo</Td>
            <Td>JOICE</Td>
          </Tr>
          <Tr>
            <Td>Técnico Virtual</Td>
            <Td>Login</Td>
            <Td>
              Utiliza as informações xpto no login para montar o dashboard do
              cliente com a visão de produtos
            </Td>
            <Td>Consumo médio esperado</Td>
            <Td>Zuul</Td>
            <Td>TECVirt</Td>
          </Tr>
        </Tbody>
      </Table>
    </Container>
  </div>
)

export default ConsumerMap
