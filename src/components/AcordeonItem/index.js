import React from 'react'

import {
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
  Container,
  Text,
  Image,
  Stack,
  Button,
  Link,
} from '@chakra-ui/react'

import Header from '../Header'

function AccordionItemBody() {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton backgroundColor='#BDBDBD' my={1}>
          <Box
            flex='1'
            textAlign='left'
            fontSize='x-large'
            fontWeight='extrabold'
          >
            Product Inventory v2
          </Box>
          <AccordionIcon />
        </AccordionButton>
      </h2>
      <AccordionPanel justifyContent='center' alignContent='center' pb={4}>
        <Container
          background='#ededed'
          maxWidth='100%'
          display='flex'
          justifyContent='space-between'
          borderRadius={4}
        >
          <Header />
        </Container>
        <Container
          maxWidth='auto'
          display='flex'
          flexDirection='column'
          justifyContent='center'
          py={5}
        >
          <Text>
            Quando a consulta de produtos do cliente é chamada, o microserviço
            ProductInventoryManagement procura primeiro por dados “cacheados”
            (persistidos) no banco local do serviço (MongoDB). Esta estratégia
            foi usada para se trazer uma melhor experiência no tempo de resposta
            do serviço, visto que a principal fonte de dados para este serviço é
            o banco de dados ODS, onde existe a posse de todos os clientes OI.
            Caso o cliente não possua dados persistidos na base local, é feita
            uma nova chamada ao serviço no banco de dados ODS, para que traga os
            dados de posse do cliete.
          </Text>
        </Container>
        <Container maxWidth='800px'>
          <Box flexDirection='column' justifyContent='center'>
            <Image
              src='https://miro.medium.com/max/940/1*uBFmEVpvj4fzF5HxLaAwMw.png'
              alt='Segun Adebayo'
            />
          </Box>
        </Container>

        <Stack
          direction='row'
          justifyContent='space-around'
          spacing={3}
          align='center'
          mt={15}
          pt={5}
        >
          <Button
            textColor='black'
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
          >
            <Link to='/infra'>Informação de Infra</Link>
          </Button>
          <Button
            w={400}
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            textColor='black'
          >
            <Link to='/models'>Modelo de dados (Collections)</Link>
          </Button>
          <Button
            w={400}
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            textColor='black'
          >
            <Link to='/environments'>URLs de Ambientes</Link>
          </Button>
        </Stack>

        <Container
          maxWidth='auto'
          display='flex'
          flexDirection='column'
          justifyContent='center'
          py={5}
        >
          <Text fontSize='large' fontWeight='extrabold' my={4}>
            ENDPOINTS
          </Text>
          <Box backgroundColor='#ededed' padding={5} borderRadius={5}>
            <Text fontSize='medium' fontWeight='bold' my={2}>
              GET
            </Text>
            <Text fontSize='medium' fontWeight='bold' my={2}>
              /customer/{'{cpf}'}/products
            </Text>
            <Text>
              is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industrys standard dummy text ever since
              the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book.
            </Text>

            <Box>
              <Stack
                direction='row'
                justifyContent='space-between'
                spacing={3}
                align='center'
                mt={15}
                pt={5}
              >
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  <Link to='/dependencies'>Mapeamento de Dependências</Link>
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  Regars de Cache
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  Mapeamento de Regras funcionais
                </Button>
              </Stack>

              <Stack
                direction='row'
                justifyContent='space-between'
                alignItems='stretch'
                spacing={3}
                align='center'
                mt={15}
                pt={5}
              >
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  Informações de Monitoramento
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  Board técnicos
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  <Link to='/changes'>Histórico de alterações</Link>
                </Button>
              </Stack>

              <Stack
                direction='row'
                justifyContent='space-between'
                spacing={3}
                align='center'
                mt={15}
                pt={5}
              >
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  <Link to='/consumers'> Mapeamento de consumidores</Link>
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  URLs no API Gateway
                </Button>
                <Button
                  w={400}
                  fontSize='medium'
                  colorScheme='teal'
                  variant='outline'
                  textColor='black'
                >
                  <Link to='/diagrams'> Diagrama de sequencia</Link>
                </Button>
              </Stack>
            </Box>
          </Box>
        </Container>

        <Container
          maxWidth='auto'
          display='flex'
          flexDirection='column'
          justifyContent='center'
          py={5}
        >
          <Text fontSize='large' fontWeight='extrabold' my={4}>
            FILAS DE CONSUMO
          </Text>
          <Box backgroundColor='#ededed' padding={5} borderRadius={5}>
            <Text fontSize='medium' fontWeight='bold' my={2}>
              RABBITMQ
            </Text>
            <Text fontSize='medium' fontWeight='bold' my={2}>
              /guarda_produtos_fila
            </Text>
            <Text>
              is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industrys standard dummy text ever since
              the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book.
            </Text>
          </Box>
        </Container>
      </AccordionPanel>
    </AccordionItem>
  )
}

export default AccordionItemBody
