import React, { useState } from 'react';
import Switch from 'react-switch';

export default ({
  line,
  column,
  handleToggle,
  handleToggleAll,
  value,
  ...props
}) => {
  const [sel, setSel] = useState(value);
  return (
    <Switch
      height={18}
      width={32}
      onChange={() => {
        if (handleToggleAll) handleToggleAll(!sel);
        if (handleToggle) handleToggle(line, column, !sel);
        setSel(!sel);
      }}
      checked={handleToggleAll ? sel : value}
      uncheckedIcon={false}
      checkedIcon={false}
      onColor="#1700FE"
      offColor="#aaa"
      handleDiameter={14}
    />
  );
};
