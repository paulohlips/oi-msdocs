import React from 'react'
import {
  Container,
  Text,
  Box,
  Stack,
  Link,
  Button,
  Input,
  Textarea,
} from '@chakra-ui/react'

function Endpoint() {
  return (
    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      backgroundColor='#ededed'
      mt={10}
      padding={5}
      borderRadius={5}
      py={5}
      px={5}
    >
      <Container maxWidth='auto' display='flex' paddingY={3} paddingX={10}>
        <Box>
          <Text pt={4} mr={2} fontWeight='black'>
            Método
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Endpoint
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Descrição
          </Text>
        </Box>

        <Box>
          <Input mt={2} placeholder='Ex: GET' />
          <Input mt={2} placeholder='Ex: /customer/{cpf}/products' />
          <Textarea mt={2} placeholder='Ex: Detalhamento do endpoint' />
        </Box>
      </Container>
      <Box display='flex' justifyContent='space-around'>
        <Stack direction='column' spacing={3} align='center' mt={15} pt={5}>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            <Link to='/dependencies'>Dependências</Link>
          </Button>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            Regras de Cache
          </Button>

          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            Regras Funcionais
          </Button>
        </Stack>

        <Stack
          direction='column'
          justifyContent='space-between'
          alignItems='stretch'
          spacing={3}
          align='center'
          mt={15}
          pt={5}
        >
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            Informações de Monitoramento
          </Button>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            Board Técnicos
          </Button>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            <Link to='/changes'>Histórico de Alterações</Link>
          </Button>
        </Stack>

        <Stack
          direction='column'
          justifyContent='space-between'
          spacing={3}
          align='center'
          mt={15}
          pt={5}
        >
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            <Link to='/consumers'>Consumidores</Link>
          </Button>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            URLs no API Gateway
          </Button>
          <Button
            fontSize='medium'
            colorScheme='teal'
            variant='outline'
            width={300}
            textColor='black'
          >
            <Link to='/diagrams'>Diagrama de Sequência</Link>
          </Button>
        </Stack>
      </Box>
    </Container>
  )
}

export default Endpoint
