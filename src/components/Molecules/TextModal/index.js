import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  bg: 'WHITE',
  pl: 'XXSMALL',
  pt: 'XXSMALL',
};

export default ({ label, percent, infoText }) => (
  <Atom.Container flexDirection="column" width={`${percent}%`}>
    <Atom.Container alignItems="flex-end">
      <Atom.Text variant="modalLabelText" ml="XXSMALL">
        {label}
      </Atom.Text>
    </Atom.Container>
    <Container>
      <Atom.Text variant="modalText" textAlign="left">
        {infoText}
      </Atom.Text>
    </Container>
  </Atom.Container>
);
