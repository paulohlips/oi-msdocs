import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import { format } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
  pl: 'XSMALL',
  pr: 'XSMALL',
  pt: 'XSMALL',
  mt: 'XXSMALL',
};

export default ({
  percent,
  label,
  obsLabel,
  onChange,
  onBlur,
  value,
  viewYearCalendar,
  minDate,
  defaultValue,
}) => {
  const [show, toggleShow] = useState(false);
  const [selectDate, setSelectDate] = useState(new Date());

  useEffect(() => {
    if (defaultValue) {
      onChange(defaultValue);
      setSelectDate(defaultValue);
    } else {
      onChange(new Date());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [defaultValue]);

  const handleSelect = (value, _) => {
    toggleShow(false);
    onChange(value);
    setSelectDate(value);
  };

  return (
    <Atom.Container flexDirection="column" width={`${percent}%`}>
      <Atom.Container alignItems="flex-end">
        <Atom.Text ml="XXSMALL">{label}</Atom.Text>
        {obsLabel && (
          <Atom.Text ml="XXSMALL" variant="details" mb="2px">
            {obsLabel}
          </Atom.Text>
        )}
      </Atom.Container>
      <Atom.Touch noSubmit onClick={() => toggleShow(!show)}>
        <Container>
          <Atom.Text textAlign="left">
            {viewYearCalendar
              ? format(selectDate || new Date(), 'yyyy', { locale: ptBR })
              : format(selectDate || new Date(), 'MMMM yyyy', { locale: ptBR })}
          </Atom.Text>
        </Container>
      </Atom.Touch>
      {show && (
        <Atom.Calendar
          onChange={handleSelect}
          value={value}
          viewYearCalendar={viewYearCalendar}
          minDate={minDate}
          defaultValue={defaultValue}
          // keepFocus={false}
        />
      )}
    </Atom.Container>
  );
};
