import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space, typography } from 'styled-system';
import Text from '../Text';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  align-items: center;
  justify-content: center;
`;

const PickerItem = styled.select`
  ${typography}
  background: transparent;
  border: 0px;
  height: 42px;
  width: 100%;
  font-family: 'Montserrat-Regular';
`;

PickerItem.defaultProps = {
  fontSize: 2,
};

Container.defaultProps = {
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  aligItems: 'center',
  bg: 'WHITE_ICE',
  pl: 'XXSMALL',
  pr: 'XSMALL',
  mt: 'XXSMALL',
};

export default ({
  name,
  showId,
  options,
  register,
  selected,
  placeholderDependency,
}) => (
  <Container>
    {Array.isArray(options) ? (
      <PickerItem ref={register} name={name} placeholder="Selecionar item">
        {options?.map((item, index) => {
          return (
            <option
              defaultValue={index === 0}
              value={item.id || item.id_contrato}
              selected={selected}
            >
              {showId
                ? `${item.id} - ${item.name}`
                : item.name || item.numero_contrato}
            </option>
          );
        })}
      </PickerItem>
    ) : (
        <Text variant="normal" pl="XXSMALL" mt="XSMALL">
          {placeholderDependency}
        </Text>
      )}
  </Container>
);
