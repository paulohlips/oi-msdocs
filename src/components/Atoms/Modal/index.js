/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout, flexbox, border } from 'styled-system';

const Modal = styled.div`
  ${space}
  ${color}
  ${layout}
  ${flexbox}
  ${border}
  display: ${(props) => (props.show ? 'flex' : 'none')};
  position: fixed;
  align-items: ${(props) => (props.list ? 'flex-end' : 'center')};
  justify-content:center;
  width: ${(props) => (props.list ? '65vw' : '120vw')};
  height: ${(props) => (props.list ? '35vw' : '120vw')};
  background: ${(props) => (props.list ? '0' : 'rgba(37, 37, 37, 0.4)')};
  box-shadow: ${(props) => (props.list ? '0' : '0 2px 5px #ccc')};
  margin-left: ${(props) => (props.list ? '20px' : '0px')};
`;

export default ({ children, show, list, handleClose, ...props }) => {
  return (
    <Modal show={show} list={list} handleClose={handleClose}>
      {children}
    </Modal>
  );
};
