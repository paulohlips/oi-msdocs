import React from 'react';
import styled from 'styled-components';

const Form = styled.form`
  width: 100%;
  height: 100%;
`;

export default ({ children, ...props }) => <Form {...props}>{children}</Form>;
