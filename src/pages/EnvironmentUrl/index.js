import React from 'react'
import {
  Text,
  // Link,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const EnvironmentUrl = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container maxWidth='100%' borderRadius={4} mt={15}>
      <Text fontSize='large' fontWeight='extrabold'>
        GET
      </Text>
      <Text>/customers/cpf/products</Text>
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={5}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        URLs de Ambientes / URLs no API Gateway
      </Text>

      <Table my={10} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Tipo
            </Th>
            <Th fontSize='medium' textColor='black'>
              Ambiente
            </Th>
            <Th fontSize='medium' textColor='black'>
              URL
            </Th>
            <Th fontSize='medium' textColor='black'>
              Dados Adicionais
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td>API Gateway Interno</Td>
            <Td>Produção</Td>
            <Td>http://..../v2/prod/..</Td>
            <Td>texto aberto...</Td>
          </Tr>
          <Tr>
            <Td>API Gateway Externo</Td>
            <Td>Produção</Td>
            <Td>HOMOLOGACAO / TI /TRG /....</Td>
            <Td>texto aberto...</Td>
          </Tr>
          <Tr>
            <Td>NetflixOSS</Td>
            <Td>Produção</Td>
            <Td>http://..../v2/prod/..</Td>
            <Td>texto aberto...</Td>
          </Tr>
        </Tbody>
      </Table>
    </Container>
  </div>
)

export default EnvironmentUrl
