import React from 'react'
import {
  Text,
  // Link,
  // Button,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const DependencyMap = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container maxWidth='100%' borderRadius={4} mt={15}>
      <Text fontSize='large' fontWeight='extrabold'>
        GET
      </Text>
      <Text>/customers/cpf/products</Text>
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Mapeamento de Dependências
      </Text>

      <Table my={10} variant='simple'>
        <Thead>
          <Tr>
            <Th fontSize='medium' textColor='black'>
              Serviço
            </Th>
            <Th fontSize='medium' textColor='black'>
              Tipo
            </Th>
            <Th fontSize='medium' textColor='black'>
              URL/Info
            </Th>
            <Th fontSize='medium' textColor='black'>
              Ordem de execução
            </Th>
            <Th fontSize='medium' textColor='black'>
              Descrição
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td>Consultar Produtos Cliente</Td>
            <Td>SOA</Td>
            <Td>http://...?wsdl</Td>
            <Td>1</Td>
            <Td>Para quem? quais informacoes? quais regras?</Td>
          </Tr>
          <Tr>
            <Td>ProductInventory</Td>
            <Td>ODS</Td>
            <Td>BD Externo</Td>
            <Td>2</Td>
            <Td>Para quem? quais informacoes? quais regras?</Td>
          </Tr>
          <Tr>
            <Td>RabbitMQ</Td>
            <Td>Fila</Td>
            <Td>nome da fila</Td>
            <Td>3</Td>
            <Td>Para quem? quais informacoes? quais regras?</Td>
          </Tr>
        </Tbody>
      </Table>
    </Container>
  </div>
)

export default DependencyMap
