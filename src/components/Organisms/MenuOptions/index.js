import React, { useState } from 'react';
import useStore from '../../../global/modalDelete/store';
import useStoreVisualize from '../../../global/modalVisualize/store';
import * as Atom from '../../Atoms';
import { MenuContainer, Button, TextButton } from './styles';

const MenuOptions = ({ data, deposit }) => {
  const [show, setShow] = useState(false);

  // zustand
  const { set, setEdit, setPdf } = useStore();
  const { setVisualize } = useStoreVisualize();

  return (
    <Atom.Touch
      width="2%"
      onClick={() => setShow(!show)}
      justifyContent="center"
    >
      <Atom.Icon icon="OPTIONS" height="5px" width="20px" />
      {show && (
        <MenuContainer>
          <Button border onClick={() => setVisualize(data)}>
            <Atom.Icon mr="XSMALL" icon="VISIBILITY" width={15} height={15} />
            <TextButton>Visualizar</TextButton>
          </Button>
          {!deposit && (
            <Button border onClick={() => setEdit(data)}>
              <Atom.Icon mr="XSMALL" icon="EDIT" width={15} height={15} />
              <TextButton>Editar</TextButton>
            </Button>
          )}
          <Button onClick={() => (deposit ? setPdf(data) : set(data))}>
            <Atom.Icon
              mr="XSMALL"
              icon={deposit ? 'DEPOSITO_BLACK' : 'DELETE'}
              width={15}
              height={15}
            />
            <TextButton>{deposit ? 'Relatório' : 'Excluir'}</TextButton>
          </Button>
        </MenuContainer>
      )}
    </Atom.Touch>
  );
};

export default MenuOptions;
