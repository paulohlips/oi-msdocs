import React from 'react'

import { Box, Container, Text, Input, Textarea } from '@chakra-ui/react'

function Queue() {
  return (
    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='row'
      justifyContent='center'
      backgroundColor='#ededed'
      padding={5}
      borderRadius={5}
      py={5}
      px={5}
    >
      <Box>
        <Text pt={4} mr={2} fontWeight='black'>
          Endpoint
        </Text>
        <Text pt={6} mr={2} fontWeight='black'>
          Descrição
        </Text>
        <Text pt={6} mr={2} fontWeight='black'>
          Método
        </Text>
        <Text pt={6} mr={2} fontWeight='black'>
          Host
        </Text>
        <Text pt={6} mr={2} fontWeight='black'>
          Database
        </Text>
        <Text pt={6} mr={2} fontWeight='black'>
          Usuário
        </Text>
      </Box>

      <Box>
        <Input mt={2} placeholder='Ex: WhatsappMessage' />
        <Textarea mt={2} placeholder='Ex: Detalhamento dos dados armazenados' />
        <Input mt={2} placeholder='Ex: Produção' />
        <Input mt={2} placeholder='Ex: digpx12' />
        <Input mt={2} placeholder='Ex: Banco de dados' />
        <Input mt={2} placeholder='Ex: user' />
      </Box>
    </Container>
  )
}

export default Queue
