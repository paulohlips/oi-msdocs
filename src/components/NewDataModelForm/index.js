import React from 'react'

import {
  Box,
  Container,
  Text,
  Textarea,
  Input,
  // IconButton,
} from '@chakra-ui/react'

function NewDataModelForm() {
  return (
    <>
      <Container display='flex' paddingY={3} paddingX={10}>
        <Box>
          <Text pt={4} mr={2} fontWeight='black'>
            Collection
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Detalhamento dos dados armazenados
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Ambiente
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Host
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Database
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Usuário
          </Text>
        </Box>

        <Box>
          <Input mt={2} placeholder='Ex: WhatsappMessage' />
          <Textarea
            mt={2}
            placeholder='Ex: Detalhamento dos dados armazenados'
          />
          <Input mt={2} placeholder='Ex: Produção' />
          <Input mt={2} placeholder='Ex: digpx12' />
          <Input mt={2} placeholder='Ex: Banco de dados' />
          <Input mt={2} placeholder='Ex: user' />
        </Box>
      </Container>
    </>
  )
}

export default NewDataModelForm
