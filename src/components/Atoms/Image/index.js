/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout, flexbox, border } from 'styled-system';
import { Imgs } from '../../../assets';

const Image = styled.img`
  ${space}
  ${color}
  ${layout}
  ${flexbox}
  ${border}
  width: 90px;
  height: 90px;
  margin-bottom: 20px;
`;

export default ({ img, ...props }) => {
  return <Image src={Imgs[img]} {...props} />;
};
