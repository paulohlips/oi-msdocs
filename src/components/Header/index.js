import React from 'react'
import { Container, Box, Text, Link } from '@chakra-ui/react'

const Header = ({ title, version }) => (
  <Container
    background='#ededed'
    maxWidth='100%'
    display='flex'
    justifyContent='space-between'
    borderRadius={4}
  >
    <Box mt={2}>
      <Text fontSize='large' fontWeight='bold'>
        {title}
      </Text>
      <Text fontSize='small' fontWeight='bold'>
        Versão {version}
      </Text>
    </Box>
    <Box mt={2}>
      <Link fontSize='large' fontWeight='bold' px={3} to='Home'>
        MOP
      </Link>
      <Link fontSize='large' fontWeight='bold' px={3} to='Home'>
        GIT
      </Link>
      <Link fontSize='large' fontWeight='bold' px={3} to='Home'>
        POSTMAN
      </Link>
      <Link fontSize='large' fontWeight='bold' px={3} to='Home'>
        PIPELINE
      </Link>
      <Link fontSize='large' fontWeight='bold' px={3} to='Home'>
        KIBANA
      </Link>
    </Box>
  </Container>
)

export default Header
