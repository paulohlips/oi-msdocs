/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout } from 'styled-system';

const BlackHole = styled.div`
  ${layout}
  ${space}
  ${color}
  height: 50px;
`;

export default ({ children, ...props }) => {
  return <BlackHole {...props}>{children}</BlackHole>;
};
