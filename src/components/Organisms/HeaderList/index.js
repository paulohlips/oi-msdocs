import React from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';

const HeaderList = ({ searchLabel, title, onClickAdd, onClickExclude }) => {
  return (
    <Atom.Container flexDirection="column" width="100%">
      <Atom.Text variant="title">{title}</Atom.Text>
      <Atom.Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        width="100%"
        mt="XBIG"
      >
        <Atom.Input px="SMALL" placeholder={searchLabel} />
        <Atom.Container flexDirection="row" alignItems="center">
          <Molecules.Button
            onClick={onClickExclude}
            variant="black"
            text="EXCLUIR"
          />
          <Molecules.Button onClick={onClickAdd} ml="XSMALL" text="ADICIONAR" />
        </Atom.Container>
      </Atom.Container>
    </Atom.Container>
  );
};

export default HeaderList;
