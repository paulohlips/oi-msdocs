/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/prop-types */
import React from 'react'
import * as Atom from '../../Atoms'
import * as Molecules from '../../Molecules'
import MenuOptions from '../MenuOptions'

/**
 * @param {array} labels - titulo das colunas
 * @param {array} data - data das colucas
 */
const Table = ({
  trigger,
  labels,
  data,
  noSelect,
  noOptions,
  selectAll,
  mt,
  onChangeValue,
  onToggle,
  onClickSelectAll,
  onClickSelectItem,
  deposit,
}) => (
  <>
    <Atom.Container
      width='100%'
      flex='1 1 auto'
      mt={mt || 'XXBIG'}
      flexDirection='column'
    >
      <Atom.Container
        px='XSMALL'
        mb='XSMALL'
        ml={2}
        width='100%'
        justifyContent='space-between'
        alignItems='flex-end'
      >
        {!noSelect && (
          <Atom.Container width='3%' alignItems='center'>
            <Atom.Select
              variant={selectAll && 'selected'}
              onClick={onClickSelectAll}
            />
          </Atom.Container>
        )}
        {labels.map((item) => (
          <Atom.Text variant='labelTable' width={item.width} pl={item.pl}>
            {item.value}
          </Atom.Text>
        ))}
        {!noSelect && !noOptions && (
          <Atom.Text width='2.5%'>
            {!noSelect && !noOptions ? 'Opt.' : ''}
          </Atom.Text>
        )}
      </Atom.Container>
      {/* --------------- content -------------*/}
      {data.map((line, indexLine) => (
        <Atom.Line
          flexDirection='row'
          alignItems='center'
          justifyContent='space-between'
          px='XSMALL'
          selected={line.selected}
        >
          {!noSelect && (
            <Atom.Container width='3%' alignItems='center'>
              <Atom.Select
                variant={line.selected && 'selected'}
                onClick={() => onClickSelectItem(indexLine)}
              />
            </Atom.Container>
          )}
          {line.columns.map((column, indexColumn) => {
            if (column.type === 'text') {
              // if (column.dependent)
              // console.log({ test: line.columns[column.dependent]?.value });
              return (
                <Atom.Text
                  width={column.width}
                  borderRight={column.border && '1px solid'}
                  variant={column.border && 'resultTable'}
                  // borderColor="white"
                  pl={column.pl}

                  // textAlign="center"
                  // lineHeight="2.5"
                  // bg="BLUE"
                  // height="36px"
                >
                  {column.dependent ? column.value : column.value}
                </Atom.Text>
              )
            }
            if (column.type === 'image') {
              return (
                <Atom.Container
                  width={column.width}
                  justifyContent='center'
                  alignItems='center'
                >
                  <Atom.Avatar variant='avatar' src={column.value} />
                </Atom.Container>
              )
            }
            if (column.type === 'status') {
              return (
                <Atom.Container width={column.width} alignItems='center'>
                  <Molecules.StatusTag active={column.value} />
                </Atom.Container>
              )
            }
            if (column.type === 'id') {
              return (
                <Atom.Container
                  bg='WHITE'
                  width={column.width}
                  alignItems='center'
                  height='99%'
                  pl='XXSMALL'
                >
                  <Atom.Text>{column.value}</Atom.Text>
                </Atom.Container>
              )
            }
            if (column.type === 'edit') {
              return (
                <Molecules.Edit
                  line={indexLine}
                  column={indexColumn}
                  percent={column.width}
                  value={column.value}
                  border={column.border}
                  onChange={onChangeValue}
                  // label="Encargos"
                  // infoText={editedAliquots(watch('contrato'), 'recisao_fgts')}
                />
              )
            }
            if (column.type === 'toggle') {
              return (
                <Atom.Container
                  width={column.width}
                  alignItems='center'
                  justifyContent='center'
                >
                  <Atom.ToggleSwitch
                    line={indexLine}
                    column={indexColumn}
                    handleToggle={onToggle}
                    value={column.value}
                  />
                </Atom.Container>
              )
            }

            return <Atom.Text width={column}>Not Text</Atom.Text>
          })}
          {!noSelect && !noOptions && (
            <MenuOptions deposit={deposit} data={line.data} />
          )}
        </Atom.Line>
      ))}
    </Atom.Container>
  </>
)

export default Table
