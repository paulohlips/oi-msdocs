/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import * as Atom from '../../Atoms';

export default ({ text, icon, retract, selected, handleClickItem }) => (
  <Atom.Touch
    bg={selected ? 'WHITE' : 'TRANSPARENT'}
    width={!retract && 210}
    onClick={handleClickItem}
  >
    <Atom.Flex ml="XSMALL">
      <Atom.Icon mr="XSMALL" icon={icon} />
      {!retract && (
        <Atom.Text variant={selected ? 'menuSelected' : 'menu'}>
          {text}
        </Atom.Text>
      )}
    </Atom.Flex>
  </Atom.Touch>
);
