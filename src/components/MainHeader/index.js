import React from 'react'
import { Link } from 'react-router-dom'

import { Container, Box, Text, Image, Button } from '@chakra-ui/react'

import { AddIcon } from '@chakra-ui/icons'

import logo from '../../assets/icons/icon-72x72.png'

const MainHeader = () => (
  <Container
    background='#ededed'
    maxWidth='100%'
    display='flex'
    justifyContent='space-between'
    borderRadius={4}
    mb={10}
    py={2}
  >
    <Link to='/'>
      <Box display='flex'>
        <Image src={logo} alt='oi-logo' />

        <Text alignSelf='center' fontSize='large' fontWeight='bold'>
          MS-Docs
        </Text>
      </Box>
    </Link>

    <Link to='/newdoc'>
      <Button
        mt='10%'
        background='#ededed'
        rightIcon={<AddIcon alignContent='center' mb={1} />}
      >
        Criar Documentação
      </Button>
    </Link>
  </Container>
)

export default MainHeader
