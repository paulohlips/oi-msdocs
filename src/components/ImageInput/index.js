import React from 'react'
import { Container, Box, Text, Input } from '@chakra-ui/react'

class InputImage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      file: null,
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
    })
  }

  render() {
    const { file } = this.state

    return (
      <Container
        display='flex'
        maxW='100%'
        flexDirection='column'
        justifyContent='flex-end'
      >
        <Box maxW='100%'>
          <Text fontWeight='bold'>Adicionar Imagem</Text>
          <Input
            type='file'
            accept='image/*'
            variant='unstyled'
            width={97}
            onChange={this.handleChange}
          />
        </Box>
        <Box alignSelf='center' mt={10}>
          {file ? <img src={file} alt='img' /> : null}
        </Box>
      </Container>
    )
  }
}
export default InputImage
