import React from 'react'

import {
  Box,
  Container,
  Text,
  Textarea,
  Input,
  // IconButton,
} from '@chakra-ui/react'

function NewEnvironmentUrlForm() {
  return (
    <>
      <Container display='flex' paddingY={3} paddingX={10}>
        <Box>
          <Text pt={4} mr={2} fontWeight='black'>
            Tipo
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Ambiente
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            URL
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Dados Adicionais
          </Text>
        </Box>

        <Box>
          <Input mt={2} placeholder='Ex: API Gateway' />
          <Input mt={2} placeholder='Ex: Produção' />
          <Input mt={2} placeholder='Ex: http://..../v2/prod/..' />

          <Textarea mt={2} placeholder='Ex: texto aberto...' />
        </Box>
      </Container>
    </>
  )
}

export default NewEnvironmentUrlForm
