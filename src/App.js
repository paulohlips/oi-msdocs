import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import theme from './styles/theme'
import Routes from './routes'
import GlobalStyle from './styles/global'
import MainHeader from './components/MainHeader'

const App = () => (
  <>
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <MainHeader />
        <Routes />
      </BrowserRouter>
      <GlobalStyle />
    </ThemeProvider>
  </>
)

export default App
