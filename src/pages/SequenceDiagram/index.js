import React from 'react'
import { Box, Text, Image, Button, Container } from '@chakra-ui/react'

import Header from '../../components/Header'

const SequenceDiagram = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container maxWidth='100%' borderRadius={4} mt={15}>
      <Text fontSize='large' fontWeight='extrabold'>
        GET
      </Text>
      <Text>/customers/cpf/products</Text>
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={5}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Diagrama de Sequência
      </Text>

      <Box>
        <Container maxWidth='800px'>
          <Box flexDirection='column' justifyContent='center'>
            <Image
              src='https://miro.medium.com/max/940/1*uBFmEVpvj4fzF5HxLaAwMw.png'
              alt='Segun Adebayo'
            />
          </Box>
        </Container>
      </Box>

      <Box alignSelf='flex-end' mt={5} mr={5}>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download PDF
        </Button>
        <Button
          fontSize='small'
          textColor='black'
          colorScheme='teal'
          variant='ghost'
        >
          Download Draw.io
        </Button>
      </Box>

      <Text>
        It is a long established fact that a reader will be distracted by the
        readable content of a page when looking at its layout. The point of
        using Lorem Ipsum is that it has a more-or-less normal distribution of
        letters, as opposed to using Content here, content here, making it look
        like readable English.It is a long established fact that a reader will
        be distracted by the readable content of a page when looking at its
        layout. The point of using Lorem Ipsum is that it has a more-or-less
        normal distribution of letters, as opposed to using Content here,
        content here, making it look like readable English.
      </Text>
    </Container>
  </div>
)

export default SequenceDiagram
