import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
  pl: 'XSMALL',
  pr: 'XSMALL',
  pt: 'XSMALL',
  mt: 'XXSMALL',
};

export default ({ obsLabel, label, percent, infoText }) => (
  <Atom.Container flexDirection="column" width={`${percent}%`}>
    <Atom.Container alignItems="flex-end">
      <Atom.Text ml="XXSMALL">{label}</Atom.Text>
      {obsLabel && (
        <Atom.Text ml="XXSMALL" variant="details" mb="2px">
          {obsLabel}
        </Atom.Text>
      )}
    </Atom.Container>
    <Container>
      <Atom.Text textAlign="left">{infoText}</Atom.Text>
    </Container>
  </Atom.Container>
);
