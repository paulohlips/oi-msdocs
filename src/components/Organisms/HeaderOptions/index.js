import React, { useState } from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';

const HeaderOptions = ({
  searchLabel,
  title,
  onClickAdd,
  onClickExclude,
  value,
}) => {
  const [selection, setSelection] = useState({
    firstSelection: false,
    secondSelection: false,
    thirdSelection: false,
    fourthSelection: false,
  });
  const [optValue, setoptValue] = useState();

  const onChange = (e) => {
    const opt = e.target.value;
    setoptValue(opt);
    console.log({ opt });
  };

  value = optValue;
  console.log({ value });

  return (
    <Atom.Container flexDirection="column" width="100%">
      <Atom.Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        width="100%"
        mt="XBIG"
      >
        <Atom.Text variant="title">{title}</Atom.Text>
        <Atom.Container flexDirection="row" alignItems="center">
          <Molecules.Button
            onClick={onClickExclude}
            variant="black"
            text="EXCLUIR"
          />
          <Molecules.Button onClick={onClickAdd} ml="XSMALL" text="ADICIONAR" />
        </Atom.Container>
      </Atom.Container>
      <Atom.Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        width="100%"
        mt="XBIG"
      >
        <Atom.Input px="SMALL" placeholder={searchLabel} />
        <Atom.Container
          flexDirection="row"
          justifyContent="flex-end"
          width="100%"
        >
          <Molecules.PickerOptions
            name="Empresa"
            label="Empresa"
            options={[
              { id: 11, name: 'Teste1' },
              { id: 111, name: 'Teste2' },
            ]}
            placeholderDependency="Carregando..."
            onChange={(e) => {
              setSelection(() => ({
                firstSelection: true,
                secondSelection: false,
                thirdSelection: false,
                fourthSelection: false,
              }));
              onChange(e);
            }}
            teste={selection.firstSelection}
          />
          <Molecules.PickerOptions
            name="Contrato"
            label="Empresa"
            options={[
              { id: 22, name: 'Teste1' },
              { id: 222, name: 'Teste2' },
            ]}
            placeholderDependency="Carregando..."
            onChange={(e) => {
              setSelection(() => ({
                firstSelection: false,
                secondSelection: true,
                thirdSelection: false,
                fourthSelection: false,
              }));
              onChange(e);
            }}
            teste={selection.secondSelection}
          />
          <Molecules.PickerOptions
            name="Colaborador"
            label="Empresa"
            options={[
              { id: 33, name: 'Teste1' },
              { id: 333, name: 'Teste2' },
            ]}
            placeholderDependency="Carregando..."
            onChange={(e) => {
              setSelection(() => ({
                firstSelection: false,
                secondSelection: false,
                thirdSelection: true,
                fourthSelection: false,
              }));
              onChange(e);
            }}
            teste={selection.thirdSelection}
          />
          <Molecules.PickerOptions
            name="Período"
            label="Empresa"
            options={[
              { id: 44, name: 'Teste1' },
              { id: 444, name: 'Teste2' },
            ]}
            placeholderDependency="Carregando..."
            onChange={(e) => {
              setSelection(() => ({
                firstSelection: false,
                secondSelection: false,
                thirdSelection: false,
                fourthSelection: true,
              }));
              onChange(e);
            }}
            teste={selection.fourthSelection}
          />
        </Atom.Container>
      </Atom.Container>
    </Atom.Container>
  );
};

export default HeaderOptions;
