/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState } from 'react'

import {
  Box,
  Container,
  Text,
  Stack,
  Button,
  Textarea,
  Input,
  IconButton,
} from '@chakra-ui/react'
import { AddIcon } from '@chakra-ui/icons'
import { Modal } from '@material-ui/core'

import ImageInput from '../../components/ImageInput'
import NewInfraComponentForm from '../../components/NewInfraComponentForm'
import NewDataModelForm from '../../components/NewDataModelForm'
import NewEnvironmentUrlForm from '../../components/NewEnvironmentUrlForm'
import Endpoint from '../../components/Endpoint'
import Queue from '../../components/Queue'

function NewAccordionItem() {
  const [infra, setInfra] = useState(false)
  const [dataModel, setDataModel] = useState(false)
  const [environmentUrl, setEnvironmentUrl] = useState(false)

  const [componentInfra, setComponentInfra] = useState([])
  const [componentDataModel, setComponentDataModel] = useState([])
  const [componentEnvironmentUrl, setComponentEnvironmentUrl] = useState([])
  const [endpoint, setEndpoint] = useState([])
  const [queue, setQueue] = useState([])

  return (
    <>
      <Container
        background='#ededed'
        maxWidth='100%'
        display='flex'
        justifyContent='space-between'
        borderRadius={4}
      />
      <Container
        maxWidth='auto'
        display='flex'
        flexDirection='column'
        justifyContent='center'
        py={5}
      >
        <Text mt={5} fontWeight='bold'>
          Nome do Microsseviço
        </Text>
        <Input />
        <Text mt={5} fontWeight='bold'>
          Versão do Microsserviço
        </Text>
        <Input />
        <Text mt={5} fontWeight='bold'>
          Descrição do Microsserviço
        </Text>
        <Textarea />

        <Text mt={5} fontWeight='bold'>
          Link GIT
        </Text>
        <Input />
        <Text mt={5} fontWeight='bold'>
          Link Kibana
        </Text>
        <Input />
        <Text mt={5} fontWeight='bold'>
          Link Pipeline
        </Text>
        <Input />
        <Text mt={5} fontWeight='bold'>
          Anexo Postman Collections
        </Text>
        <Input type='file' multiple variant='unstyled' width={103} />
        <Text mt={5} fontWeight='bold'>
          Anexo MOP
        </Text>
        <Input type='file' multiple variant='unstyled' width={103} />
      </Container>

      <Container
        display='flex'
        justifyContent='flex-end'
        maxWidth='100%'
        maxH='auto'
      >
        <ImageInput name='avatar_id' alt='architecture' />
      </Container>

      <Stack
        direction='row'
        justifyContent='space-around'
        spacing={3}
        align='center'
        mt={15}
        pt={5}
      >
        <Button
          textColor='black'
          fontSize='medium'
          colorScheme='teal'
          variant='outline'
          onClick={() => setInfra(true)}
        >
          <Text>Informação de Infra</Text>
        </Button>
        <Button
          fontSize='medium'
          colorScheme='teal'
          variant='outline'
          textColor='black'
          onClick={() => setDataModel(true)}
        >
          <Text>Modelo de dados (Collections)</Text>
        </Button>
        <Button
          fontSize='medium'
          colorScheme='teal'
          variant='outline'
          textColor='black'
          onClick={() => setEnvironmentUrl(true)}
        >
          <Text>URLs de Ambientes</Text>
        </Button>
      </Stack>

      {infra ? (
        <Modal
          open={infra}
          onClose={infra}
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          style={{ overflow: 'scroll' }}
        >
          <>
            <Container
              background='white'
              paddingY={5}
              paddingX={5}
              mt={10}
              borderRadius={10}
            >
              <Text
                display='flex'
                justifyContent='center'
                fontSize='larger'
                fontWeight='extrabold'
                mt={3}
              >
                Informações de Infraestrutura
              </Text>
              <Container mt={5}>
                <ImageInput name='avatar_id' alt='architecture' />
              </Container>
              <Container display='flex' justifyContent='space-around' mt={5}>
                <Box>
                  <Text fontWeight='bold'>Anexar PDF</Text>
                  <Input type='file' multiple variant='unstyled' width={103} />
                </Box>
                <Box>
                  <Text fontWeight='bold'>Anexar Draw.io</Text>
                  <Input type='file' multiple variant='unstyled' width={103} />
                </Box>
              </Container>
              <Container display='flex' mt={5} alignContent='space-between'>
                <Text mt={10} fontSize='large' fontWeight='bold'>
                  Componentes
                </Text>
              </Container>
              <NewInfraComponentForm />
              {componentInfra.map((item) => (
                <NewInfraComponentForm key={item} />
              ))}
              <Container display='flex' justifyContent='center'>
                <IconButton
                  ml={3}
                  w={10}
                  h={10}
                  alignSelf='end'
                  aria-label='Search database'
                  icon={<AddIcon width={3} />}
                  onClick={() =>
                    setComponentInfra((prevState) => [
                      ...prevState,
                      Math.random(),
                    ])
                  }
                />
              </Container>
              <Container mt={5} display='flex' justifyContent='flex-end'>
                <Button mr={4} onClick={() => setInfra(false)}>
                  Salvar
                </Button>
                <Button onClick={() => setInfra(false)}>Fechar</Button>
              </Container>
            </Container>
          </>
        </Modal>
      ) : null}

      {dataModel ? (
        <Modal
          open={dataModel}
          onClose={dataModel}
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          style={{ overflow: 'scroll' }}
        >
          <>
            <Container
              background='white'
              paddingY={5}
              paddingX={5}
              mt={10}
              borderRadius={10}
            >
              <Text
                display='flex'
                justifyContent='center'
                fontSize='larger'
                fontWeight='extrabold'
                mt={3}
              >
                Modelos de Dados (Collections)
              </Text>

              <Container mt={5}>
                <ImageInput name='avatar_id' alt='architecture' />
              </Container>

              <Container display='flex' justifyContent='space-around' mt={5}>
                <Box>
                  <Text fontWeight='bold'>Anexar PDF</Text>
                  <Input type='file' multiple variant='unstyled' width={103} />
                </Box>
                <Box>
                  <Text fontWeight='bold'>Anexar Draw.io</Text>
                  <Input type='file' multiple variant='unstyled' width={103} />
                </Box>
              </Container>

              <Container display='flex' mt={5} alignContent='space-between'>
                <Text mt={10} fontSize='large' fontWeight='bold'>
                  Collections
                </Text>
              </Container>

              <NewDataModelForm />

              {componentDataModel.map((item) => (
                <NewDataModelForm key={item} />
              ))}

              <Container display='flex' justifyContent='center'>
                <IconButton
                  ml={3}
                  w={10}
                  h={10}
                  alignSelf='end'
                  aria-label='Search database'
                  icon={<AddIcon width={3} />}
                  onClick={() =>
                    setComponentDataModel((prevState) => [
                      ...prevState,
                      Math.random(),
                    ])
                  }
                />
              </Container>

              <Container mt={5} display='flex' justifyContent='flex-end'>
                <Button mr={4} onClick={() => setDataModel(false)}>
                  Salvar
                </Button>
                <Button onClick={() => setDataModel(false)}>Fechar</Button>
              </Container>
            </Container>
          </>
        </Modal>
      ) : null}

      {environmentUrl ? (
        <Modal
          open={environmentUrl}
          onClose={environmentUrl}
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          style={{ overflow: 'scroll' }}
        >
          <>
            <Container
              background='white'
              paddingY={5}
              paddingX={5}
              mt={10}
              borderRadius={10}
            >
              <Text
                display='flex'
                justifyContent='center'
                fontSize='larger'
                fontWeight='extrabold'
                mt={3}
              >
                URLs de Ambientes
              </Text>

              <Container display='flex' mt={5} alignContent='space-between'>
                <Text mt={10} fontSize='large' fontWeight='bold'>
                  Collections
                </Text>
              </Container>

              <NewEnvironmentUrlForm />

              {componentEnvironmentUrl.map((item) => (
                <NewEnvironmentUrlForm key={item} />
              ))}

              <Container display='flex' justifyContent='center'>
                <IconButton
                  ml={3}
                  w={10}
                  h={10}
                  alignSelf='end'
                  aria-label='Search database'
                  icon={<AddIcon width={3} />}
                  onClick={() =>
                    setComponentEnvironmentUrl((prevState) => [
                      ...prevState,
                      Math.random(),
                    ])
                  }
                />
              </Container>

              <Container mt={5} display='flex' justifyContent='flex-end'>
                <Button mr={4} onClick={() => setEnvironmentUrl(false)}>
                  Salvar
                </Button>
                <Button onClick={() => setEnvironmentUrl(false)}>Fechar</Button>
              </Container>
            </Container>
          </>
        </Modal>
      ) : null}

      <Container
        mt={10}
        height={100}
        maxHeight={1}
        maxWidth='100%'
        backgroundColor='#ededed'
      />

      {endpoint.map((item) => (
        <Endpoint key={item} />
      ))}

      <Container mt={10} display='flex' maxWidth='100%' alignSelf='flex-start'>
        <Text>Adicionar Endpoint</Text>
        <IconButton
          ml={3}
          w={10}
          h={10}
          alignSelf='end'
          aria-label='Search database'
          icon={<AddIcon width={3} />}
          onClick={() =>
            setEndpoint((prevState) => [...prevState, Math.random()])
          }
        />
      </Container>

      <Container
        mt={10}
        height={100}
        maxHeight={1}
        maxWidth='100%'
        backgroundColor='#ededed'
      />

      {queue.map((item) => (
        <Queue key={item} />
      ))}

      <Container mt={10} display='flex' maxWidth='100%' alignSelf='flex-start'>
        <Text>Adicionar Fila</Text>
        <IconButton
          ml={3}
          w={10}
          h={10}
          alignSelf='end'
          aria-label='Search database'
          icon={<AddIcon width={3} />}
          onClick={() => setQueue((prevState) => [...prevState, Math.random()])}
        />
      </Container>
    </>
  )
}

export default NewAccordionItem
