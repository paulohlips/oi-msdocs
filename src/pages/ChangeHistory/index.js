import React from 'react'
import {
  Box,
  Container,
  Text,
  // Link,
} from '@chakra-ui/react'

import Header from '../../components/Header'

const ChangeHistory = () => (
  <div>
    <Container
      background='#ededed'
      maxWidth='100%'
      display='flex'
      justifyContent='space-between'
      borderRadius={4}
    >
      <Header title='Product Inventory' version={2.5} />
    </Container>

    <Container maxWidth='100%' borderRadius={4} mt={15}>
      <Text fontSize='large' fontWeight='extrabold'>
        GET
      </Text>
      <Text>/customers/cpf/products</Text>
    </Container>

    <Container
      maxWidth='auto'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      py={5}
    >
      <Text mt={2} fontSize='large' fontWeight='extrabold' my={4}>
        Histórico de Alterações
      </Text>
      <Box mt={2} backgroundColor='#ededed' padding={5} borderRadius={5}>
        <Text fontSize='medium' fontWeight='bold' my={2}>
          Integração com mongoDB
        </Text>
        <Text fontSize='small' fontWeight='medium' my={2}>
          industry. Lorem Ipsum has been the industrys standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing L
        </Text>
        <Box
          mt={5}
          display='flex'
          flexDirection='row'
          justifyContent='space-between'
        >
          <Box display='flex' flexDirection='row'>
            <Text fontWeight='bold'>Ambiente:</Text>
            <Text ml={1}>Produção</Text>
          </Box>
          <Text fontSize='smaller'>24/03/2021 11:25:30</Text>
        </Box>
      </Box>

      <Box mt={2} backgroundColor='#ededed' padding={5} borderRadius={5}>
        <Text fontSize='medium' fontWeight='bold' my={2}>
          Criação de Enpoint para OI Play
        </Text>
        <Text fontSize='small' fontWeight='medium' my={2}>
          industry. Lorem Ipsum has been the industrys standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing L
        </Text>
        <Box
          mt={5}
          display='flex'
          flexDirection='row'
          justifyContent='space-between'
        >
          <Box display='flex' flexDirection='row'>
            <Text fontWeight='bold'>Ambiente:</Text>
            <Text ml={1}>Produção</Text>
          </Box>
          <Text fontSize='smaller'>24/03/2021 11:25:30</Text>
        </Box>
      </Box>
    </Container>
  </div>
)

export default ChangeHistory
