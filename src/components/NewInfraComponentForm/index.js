import React from 'react'

import {
  Box,
  Container,
  Text,
  Textarea,
  Input,
  // IconButton,
} from '@chakra-ui/react'

function NewInfraComponentForm() {
  return (
    <>
      <Container display='flex' paddingY={3} paddingX={10}>
        <Box>
          <Text pt={4} mr={2} fontWeight='black'>
            Compoente
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Tipo
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Ambiente
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Host
          </Text>
          <Text pt={6} mr={2} fontWeight='black'>
            Detalhamento
          </Text>
        </Box>

        <Box>
          <Input mt={2} placeholder='Ex: MongoDB' />
          <Input mt={2} placeholder='Ex: Banco de dados' />
          <Input mt={2} placeholder='Ex: Produção' />
          <Input mt={2} placeholder='Ex: digpx12' />

          <Textarea mt={2} placeholder='Ex: Detalhes, usuários, etc' />
        </Box>
      </Container>
    </>
  )
}

export default NewInfraComponentForm
