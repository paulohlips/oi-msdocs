import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Home from '../pages/Home'
import ChangeHistory from '../pages/ChangeHistory'
import DataModel from '../pages/DataModel'
import Infra from '../pages/InfraInfo'
import ConsumerMap from '../pages/ConsumerMap'
import EnvironmentUrl from '../pages/EnvironmentUrl'
import DependencyMap from '../pages/DependencyMap'
import SequenceDiagram from '../pages/SequenceDiagram'
import NewAccordionItem from '../pages/NewAccordionItem'

const Routes = () => (
  <Switch>
    <Route path='/' exact component={Home} />
    <Route path='/changes' component={ChangeHistory} />
    <Route path='/models' component={DataModel} />
    <Route path='/infra' component={Infra} />
    <Route path='/consumers' component={ConsumerMap} />
    <Route path='/environments' component={EnvironmentUrl} />
    <Route path='/dependencies' component={DependencyMap} />
    <Route path='/diagrams' component={SequenceDiagram} />
    <Route path='/newdoc' component={NewAccordionItem} />
  </Switch>
)

export default Routes
